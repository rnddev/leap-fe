$(document).ready(function() {
    const curriculum_bar_height = 2; // 2 = height + margin-bottom
    const curriculum2 = $(".curriculum:nth-child(2)");
    const curriculumBar = $(".curriculum-bar");
    const curriculumFirst = $(".curriculum:first-child");
    const ROTATE_CSS_CLASS = "triangle-rotate";
    const GAP_REMOVER_CSS = "gap-remover";
    const ORIGINAL_HEIGHT_CSS = "original-height";
    const style = document.documentElement.style;
    let isBarClick = false;
    curriculumFirst.click(function() {
      if (!isBarClick) {
        curriculumFirst.addClass(ROTATE_CSS_CLASS);
        curriculum2.addClass(GAP_REMOVER_CSS);
        setHeightWithChild(curriculumBar.children().length);
        curriculumBar.addClass(ORIGINAL_HEIGHT_CSS);
        isBarClick = true;
      } else {
        curriculumFirst.removeClass(ROTATE_CSS_CLASS);
        curriculum2.removeClass(GAP_REMOVER_CSS);
        setHeightWithChildRemove();
        curriculumBar.removeClass(ORIGINAL_HEIGHT_CSS);
        isBarClick = false;
      }
    });
  
    function setHeightWithChild(childrenCount) {
      style.setProperty(
        "--curriculum-bar-height",
        curriculum_bar_height * childrenCount + "rem"
      );
    }
  
    function setHeightWithChildRemove() {
      style.setProperty("--curriculum-bar-height", curriculum_bar_height + "rem");
    }
  });
  
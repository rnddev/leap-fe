let setCookieName = (name,val, expires) => {
    document.cookie = name+'='+val+';expires='+expires+'; path=/'//; HttpOnly'//; Secure
}

let setCookieAuth = (token, expires) => {
    document.cookie = 'token='+token+';expires='+expires+'; path=/'//; HttpOnly'//; Secure
}

let revokeCookie = (name) => {
    document.cookie = name+'=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/'//; HttpOnly'//; HttpOnly'
}

let getCookie = (name) => {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

let revokeCookieAuth = () => {
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/'
    revokeCookie()
}

let redirectTo = (endpoint) =>{
    window.location.href = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+endpoint;
}

let urlTo = (endpoint) => {
    return window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+endpoint;
}

let config = {
    unauthenticated: {
        headers: {
            'Content-Type': "application/json",
        }
    },
    auth: {
        headers: {
            'Content-Type': "application/json",
            'Authorization': 'Bearer ' + getCookie('token')
        }
    },
    'auth-with-file': {
        headers: {
            'Content-Type': "application/json",
            'Authorization': 'Bearer ' + getCookie('token'),
            'Content-Type': 'multipart/form-data'
        }
    }
};

export {config, revokeCookie, redirectTo, urlTo, setCookieName, setCookieAuth, revokeCookieAuth, getCookie}
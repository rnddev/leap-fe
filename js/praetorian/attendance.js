import { lecturer,route } from "../service/url.js";
import {config} from '../service/config.js'
import {logout} from '../service/auth.js'
import {isLecturer} from '../service/middleware.js'
import {redirectTo} from '../service/config.js'

const submitAttendanceData = () => {
    var attend_member_id = [];
    $(':checkbox:checked').each(function(i){
          attend_member_id[i] = $(this).val();
    });
    var absent_member_id = [];
    $(':checkbox:not(:checked)').each(function(i){
          absent_member_id[i] = $(this).val();
    });
    
    let class_schedule_id = $("#class_schedule_id").val()
    let user_class_id = $("#user_class_id").val()
    axios.post(lecturer["insert"], {
        'attend_member_id': attend_member_id,
        'absent_member_id': absent_member_id,
        'class_schedule_id': class_schedule_id,
        'user_class_id': user_class_id
	}, config.auth)
	.then(function (response) {
        alert(response.data.message)
        redirectTo(route.praetorian["view-class"])
	})
	.catch(function (error) {
        console.log(error)
        redirectTo(route.praetorian["view-class"])
	});
}

const clearCheckbox = () => {
    $('input[type=checkbox]').each(function(){ 
        this.checked = false; 
    }); 
}

const generateTableContents = (index,id,member_id,name) => {
    return `<tr>
    <td>${index+1}</td>
    <td class="hide">${member_id}</td>
    <td>${name}</td>
    <td><input type="checkbox" id="member_id[${index}]" value="${id}">
    </td>
    </tr>`
}

const getClassAbsences = () => {
	axios.get(lecturer["attendance"], 
		config.auth
	)
		.then((response) => {
        let contents = ""
        
        for (let i=0; i<response.data.members.length; i++) {
            let id = response.data.members[i].id;
            let member_id = response.data.members[i].user_id;
            let name = response.data.members[i].name;
            contents += generateTableContents(i, id, member_id, name);
        }
        // let class_id = response.data.class_id;
        // let input_hidden = `<input type="hidden" value="${class_id}" id="user_class_id">`

        $("#table-contents").html(contents)
        // $("#input-hidden").html(input_hidden)

	}).catch(function (error) {
		console.log(error);
	});
}

const getNextClassScheduleId = () => {
	axios.get(lecturer["class-list"], 
		config.auth
	)
		.then((response) => {
        let class_schedule_id = response.data.lecturer_classes[0].next_classes[0].id;
        let input_hidden = `<input type="hidden" value="${class_schedule_id}" id="class_schedule_id">`
        $("#input-hidden2").html(input_hidden)
	}).catch(function (error) {
		console.log(error);
	});
}

$(document).ready(function () {
    if(!isLecturer()){
        alert("You Are Not Allowed to Access This Page")
        logout()
    } else {
        getClassAbsences()
        getNextClassScheduleId()
        $('#btn-logout').click(function() {
            logout()
        });
        $('#btn-save-attendance').click(submitAttendanceData);
        $('#btn-clear-checkbox').click(clearCheckbox);
    }
    $('#bar').click(function() {
        $('.navItemContainer').toggleClass('activeBar');
    });
});